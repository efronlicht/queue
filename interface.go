package queue

type (
	// Stacker is a growable stack, such as a Deque or Stack. Last in, First Out.
	Stacker[T any] interface {
		// Pop the most recently pushed item back off the stack (if any). Bounds: Len() > 0 iff ok.
		PopBack() (t T, ok bool)
		// Push an item T on to the back of the stack.
		PushBack(T)
		// Len is the number of items remaining in the stack. Bounds(): Len >= 0.
		Len() int
	}
	// Queuer is a growable queue, such as a Deque. First in, First Out.
	Queuer[T any] interface {
		// PushBack appends an item T on to the back of the queue.
		PushBack(T)
		// PopFront removes the first item queued from the front. Bounds: Len() > 0 iff ok
		PopFront() (t T, ok bool)
		// Len is the number of items remaining in the queue. Bounds: Len() >= 0.
		Len() int
	}
	// Dequer is a data structure that can be added to from either end.
	// An item popped from either end may or may not be recently added.
	Dequer[T any] interface {
		// PushBack appends an item T on to the back of the deque.
		PushBack(T)
		// PushBack enqueues an item T on to the front of the deque.
		PushFront(T)
		// PopFront removes the item in the front. Bounds: Len() > 0 iff ok
		PopFront() (t T, ok bool)
		// PopBack removes the item in the back. Bounds: Len() > 0 iff ok
		PopBack() (t T, ok bool)
		// Len is the number of items remaining in the deque. Bounds: Len() >= 0.
		Len() int
	}
	// Ringer is a circular, fixed-length double-ended queue, such as a Ring
	// Pushes and pops to one end of a Ringer evict items from the other end to make room if necessary.
	Ringer[T any] interface {
		// Len is number of items remaining in the Ring. Bounds: 0 <= Len <= Cap.
		Len() int
		// Cap is the maximum capacity of the of the ring. Bounds: Cap >= 0.
		Cap() int
		// PushBack a T, removing an item fromFront if necessary. Bounds: Len() < Cap iff ok.
		PushBack(t T) (fromFront T, ok bool)
		// PushFront a T, removing an item fromBack if necessary. Bounds: Len() < Cap iff ok.
		PushFront(t T) (fromBack T, ok bool)
		// PopBack removes an item from the back, if possible.  Bounds: Len() > 0 iff ok
		PopBack() (t T, ok bool)
		// PopFront removes an item from the front, if possible. Bounds: Len() > 0 iff ok
		PopFront() (t T, ok bool)
	}
)
