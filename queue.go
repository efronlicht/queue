// Package queue implements a fixed-size double-ended queue, queue.Ring, and
// a growable double-ended queue, queue.Deque, and a simple stack, queue.Stack.
package queue

import (
	"fmt"
)

// a brief mathematical note:
// in go,  n < 0 means n % m < 0.
// but we want a positive modulus.
// so you will see the construction (n + m) % m, to force a positive result (at least, for -m < n).

// SliceDeque is a double-ended queue (sometimes called a 'Deque') with O(1) pushes and pops from the front and back.
type (

	// Deque is a double-ended, growable queue with 0(1) insertion and removal from either end.
	// The zero value is OK for immediate use.
	Deque[T any] struct{ deque[T] }
	// Stack is a growable stack made from a slice, with O(1) pushes and pops to from the back.
	Stack[T any] []T
	// Ring is a ringbuffer with fixed capacity and 0(1) insertion and removal from either end.
	// The zero value is not valid, ever. Make one with NewFixed.
	Ring[T any] struct{ deque[T] }
	// deque contains the shared logic and structure between SliceDeque and SliceRing
	deque[T any] struct {
		s    []T
		f, b int
	}
)

var _ Dequer[struct{}] = (*Deque[struct{}])(nil)
var _ Ringer[struct{}] = (*Ring[struct{}])(nil)

func NewFixed[T any](cap int) *Ring[T] {
	if cap <= 0 {
		panic(fmt.Errorf("expected a capacity > 0, but got %d", cap))
	}
	return &Ring[T]{deque[T]{s: make([]T, cap)}}
}
func NewGrowable[T any](startingCap int) *Deque[T] {
	if startingCap < 0 {
		panic(fmt.Errorf("expected a capacity >= 0, but got %d", startingCap))

	}
	return &Deque[T]{deque[T]{s: make([]T, 0, startingCap)}}
}

type _t struct{}

var (
	_ Dequer[_t]  = (*Deque[_t])(nil)
	_ Stacker[_t] = (*Deque[_t])(nil)
	_ Queuer[_t]  = (*Deque[_t])(nil)
	_ Stacker[_t] = (*Stack[_t])(nil)
)

// AsSlice rearranges the deque so that it is laid out in memory, starting at index 0.
// and returns a view into that slice. The order of the elements is preserved.
// Any modifications to that slice may break the underlying deque. You probably want CloneSlice.
func (d *deque[T]) AsSlice() []T {
	switch {
	case d.f == 0: // already a linear slice.
		return d.s[d.f:d.b]
	case d.f == d.b: // empty. reslice to preserve capacity.
		return d.s[:0]
	case d.f < d.b: // already linear.  we shift everything to the front so not to waste capacity.
		len := d.b - d.f
		copy(d.s[0:len], d.s[d.f:d.f+len])
		return d.s[:len]
	default: // wrapping: rotate to linearize.
		n := len(d.s) - d.f + d.b
		for i := 0; i < n; i++ {
			j := (d.f + i) % len(d.s)
			d.s[i], d.s[j] = d.s[j], d.s[i]
		}
		return d.s[:n]
	}
}

// CloneSlice returns a copy of the contents of the queue as a slice, in the same order.
func (d deque[T]) CloneSlice() []T {
	switch {
	case d.f == 0, d.f == d.b:
		return nil
	case d.f < d.b: // already linear.
		dst := make([]T, d.b-d.f)
		copy(dst, d.s[d.f:d.b])
		return dst
	default: // wrapping: rotate to linearize.
		dst := make([]T, len(d.s)-d.f+d.b)
		n := copy(dst, d.s[d.f:])
		copy(dst[n:], d.s[:d.b])
		return dst
	}
}

// Len is the current length of the Deque.
func (d *deque[T]) Len() int {
	switch {
	case d.f == d.b: // empty
		return 0
	case d.f < d.b: // linear.
		return d.b - d.f
	default: // wrapping
		return (len(d.s) - d.f) + d.b
	}
}

func (d *deque[T]) PopBack() (t T, ok bool) {
	if d.f == d.b {
		return t, false
	}
	d.b = (d.b - 1 + len(d.s)) % len(d.s) // shift towards the front, wrapping if necessary.
	return d.s[d.b], true

}
func (d *deque[T]) PopFront() (t T, ok bool) {
	if d.f == d.b { // empty
		return t, false
	}
	t = d.s[d.f]
	d.f = (d.f + 1) % len(d.s) // shift towards the back, wrapping if necessary.
	return t, true

}

// PushFront always succeeds for a SliceDeque.
func (q *Deque[T]) PushFront(t T) {
	switch {
	case q.f == q.b: // empty: might as well linearize.
		*q = Deque[T]{deque[T]{s: append(q.s[:0], t), f: 0, b: 1}}
	case q.Len() == len(q.s): // full: reallocate.
		*q = Deque[T]{deque[T]{
			s: append(append(make([]T, 0, len(q.s)*2), t), q.AsSlice()...),
			f: 0,
			b: len(q.s) + 1,
		}}
	default: // extra room in the front.
		q.f = (q.f - 1 + len(q.s)) % len(q.s) // shift backwards, wrapping if necessary.
		q.s[q.f] = t
	}
}
func (q *Deque[T]) PushBack(t T) {
	if q.f == q.b || q.Len() == len(q.s) {
		*q = DequeFromSlice(q.AsSlice())
		return
	}

	q.s[q.b] = t
	q.b = (q.b + 1) % len(q.s)

}

func (s *Stack[T]) PushBack(t T) { *s = append(*s, t) }
func (s *Stack[T]) PopBack() (t T, ok bool) {
	n := s.Len()
	if n == 0 {
		return t, false
	}
	n--
	t = (*s)[n]
	*s = (*s)[:n]
	return t, true
}
func (s *Stack[T]) Len() int { return len(*s) }

// PushBack a T, removing an item fromFront if necessary.

func (q *Ring[T]) PushBack(t T) (fromFront T, ok bool) {
	if q.f == q.b {
		q.s[q.f], t = t, q.s[q.f]
		q.f = (q.f + 1) % len(q.s)
		q.b = (q.b + 1) % len(q.s)
		return t, false
	}
	q.s[q.b] = t
	q.b = (q.b + 1) % len(q.s)
	return t, true
}
func (q *Ring[T]) Cap() int { return len(q.s) }
func (q *Ring[T]) PushFront(t T) (fromFront T, ok bool) {
	if q.Len() == q.Cap() { // evict the item in the back to make room.
		q.s[q.f], t = t, q.s[q.f]
		q.b = (q.b - 1 + len(q.s)) % len(q.s)
		q.f = (q.f - 1 + len(q.s)) % len(q.s)

		return t, false
	}
	q.s[q.b] = t
	q.f = (q.f + 1) % len(q.s)
	return t, true
}

func DequeFromSlice[T any](s []T) Deque[T] {
	return Deque[T]{deque[T]{s: s, f: 0, b: len(s)}}
}

// FixedFromSlice converts the slice to a deque.FixedSize. It will panic if len(s) > cap.
func FixedFromSlice[T any](s []T, cap int) Ring[T] {
	if len(s) > cap {
		panic(fmt.Errorf("can't create a fixed-size deque of len %d from a slice of len %d: too big!", cap, len(s)))
	}
	return Ring[T]{deque[T]{s: s, f: 0, b: len(s)}}

}
